---------------------------------------------------------------------------------------------------
Version: 0.4.0
Date: Nov 30, 2020
  Changes:
    - Version bump.  Still works as intended, but now on Factorio 1.1.

---------------------------------------------------------------------------------------------------
Version: 0.3.1
Date: Jun 13, 2020
  Changes:
    - Try to avoid reported crash regarding global.death_markers having not been created properly.  Unable to reproduce, so this might still be an issue.

---------------------------------------------------------------------------------------------------
Version: 0.3.0
Date: Jan 22, 2020
  Changes:
    - Updated to run on 0.18.

---------------------------------------------------------------------------------------------------
Version: 0.2.0
Date: May 5, 2019
  Changes:
    - Fix edge case where corpse is mined, but only partially looted.  In such a case, the corpse would still be there, just not with everything it had prior.

---------------------------------------------------------------------------------------------------
Version: 0.1.2
Date: Apr 19, 2019
  Changes:
    - Add TR locale.

---------------------------------------------------------------------------------------------------
Version: 0.1.1
Date: Mar 1, 2019
  Changes:
    - Update for 0.17.

---------------------------------------------------------------------------------------------------
Version: 0.1.0
Date: Dec 18, 2017
  Changes:
    - Initial release.
